const express = require('express');
const sql = require("mssql");
const bodyParser = require('body-parser');
const connection = require("../connection/connection")();
const router = express.Router();
router.use(bodyParser.json());

const routes = function () {
    router.route('/booking')                //gets all the booking data from booking table
        .get(function (req, res) {
            connection.connect().then(function () {
                let sqlQuery = "SELECT * FROM booking ORDER BY BookingId DESC";
                let request = new sql.Request(connection);
                request.query(sqlQuery).then(function (recordset) {
                    res.json(recordset.recordset);
                    connection.close();
                })
                    .catch(function (err) {
                        connection.close();
                        res.status(400).send("Database Error while getting data ");
                    });
            })
                .catch(function (err) {
                    connection.close();
                    res.status(400).send("Connection Error while getting data " + req.body.Passengers);
                });
        });

    router.route('/booking/top5/')    //getting top 5 most recent bookings for a user 
        .get(function (req, res) {
            connection.connect().then(function () {
                let sqlQuery = "select top 5 * from booking order by DateOut DESC;";
                let request = new sql.Request(connection);
                request.query(sqlQuery).then(function (recordset) {
                    res.json(recordset.recordset);
                    connection.close();
                })
                    .catch(function (err) {
                        connection.close();
                        res.status(400).send("Database Error while getting data ");
                    });
            })
                .catch(function (err) {
                    connection.close();
                    res.status(400).send("Connection Error while getting data " + req.body.Passengers);
                });
        });


    router.route('/')                          // updating cancelled booking status
        .put(function (req, res) {
            connection.connect().then(function () {
                let transaction = new sql.Transaction(connection);
                transaction.begin().then(function () {
                    let request = new sql.Request(transaction);
                    let BookingId = request.input("BookingId", sql.Int(), req.body.BookingId);
                    request.query("update booking set cancelled=1 where BookingId=@BookingId;").then(function () {
                        transaction.commit().then(function (recordSet) {
                            connection.close();
                            res.status(200).send(req.body);
                        }).catch(function (err) {
                            connection.close();
                            res.status(400).send("Transaction Commit Error while updating data");
                        });
                    }).catch(function (err) {
                        connection.close();
                        res.status(400).send("Database Error while inserting data");
                    });
                }).catch(function (err) {
                    connection.close();
                    res.status(400).send("Transaction Begin Error while inserting data");
                });
            }).catch(function (err) {
                connection.close();
                res.status(400).send("Connection Error while inserting data");
            });
        });


    router.route('/booking')          //posting the values to database
        .post(function (req, res) {
            connection.connect().then(function () {
                let transaction = new sql.Transaction(connection);
                transaction.begin().then(function () {
                    let request = new sql.Request(connection);
                    let passenger = request.input("Passengers", sql.Int(), req.body.Passengers);
                    let dateout = request.input("DateOut", sql.Date(), req.body.DateOut);
                    let dateback = request.input("DateBack", sql.Date(), req.body.DateBack);
                    let FlightOutId = request.input("FlightOutId", sql.VarChar(6), req.body.FlightOutId);
                    let FlightBackId = request.input("FlightBackId", sql.VarChar(6), req.body.FlightBackId);
                    let ReturnOneway = request.input("ReturnOneway", sql.Bit(), req.body.ReturnOneway);
                    let UserId = request.input("UserId", sql.Int(), req.body.UserId);
                    let BookingPrice = request.input("BookingPrice", sql.Float(), req.body.BookingPrice);
                    let PaymentId = request.input("PaymentId", sql.VarChar(50), req.body.PaymentId);
                    //request.input("Cancelled", req.body.Cancelled);
                    let todayDate = new Date();
                    let outDate = new Date(dateout.parameters.DateOut.value);
                    let inDate = new Date(dateback.parameters.DateBack.value);

                    console.log(dateout.parameters.DateOut.value);
                    console.log(dateout.parameters.DateOut.value.valueOf < todayDate.valueOf);
                    console.log(todayDate);
                    if (passenger.parameters.Passengers.value === null || FlightOutId.parameters.FlightOutId.value === null ||ReturnOneway.parameters.ReturnOneway.value === null||UserId.parameters.UserId.value === null||BookingPrice.parameters.BookingPrice.value === null||PaymentId.parameters.PaymentId.value === null|| passenger.parameters.Passengers.value >= 20 || dateout.parameters.DateOut.value === null || outDate.getTime() < todayDate.getTime() ) {

                        connection.close();
                        res.status(401).send("Forbidden to insert record as one of the values was null")
                    }

                    else {
                        request.query("INSERT INTO booking(DateOut, DateBack, Passengers,FlightOutId, FlightBackId, ReturnOneway, UserId, BookingPrice, PaymentId) VALUES(@DateOut , @DateBack, @Passengers,@FlightOutId, @FlightBackId, @ReturnOneway, @UserId, @BookingPrice, @PaymentId)").then(function () {
                            //request.execute("spAddShipper").then(function () { // Use a Stored Procedure
                            transaction.commit().then(function (recordSet) {
                                connection.close();
                                res.status(200).send(req.body);
                            }).catch(function (err) {
                                connection.close();
                                res.status(400).send("Transaction Commit Error while inserting data");
                            });
                        }).catch(function (err) {
                            connection.close();
                            res.status(400).send("Database Error while inserting data");
                            console.log(err);
                        });

                    }

                }).catch(function (err) {
                    connection.close();
                    console.log(err);
                    res.status(400).send("Connection Error while inserting data");
                });
            });
        });

    router.route('/booking/bookingId/:bookingid')    //getting booking by particular booking ID
        .get(function (req, res) {
            let BookingId = req.params.bookingid;
            connection.connect().then(function () {
                let sqlQuery = "SELECT * FROM dbo.Booking WHERE BookingId = @BookingId";
                let req = new sql.Request(connection);
                req.input("BookingId", sql.Int(5), BookingId);
                req.query(sqlQuery).then(function (recordset) {
                    res.json(recordset.recordset);
                    connection.close();
                })

                    .catch(function (err) {
                        connection.close();
                        res.status(400).send("Database Error while getting data");
                        console.log(err);
                    });
            })
                .catch(function (err) {
                    connection.close();
                    res.status(400).send("Connection Error while getting data " + err);
                });
        });


    router.route('/booking/cancelled')               //getting list of all the cancelled bookings
        .get(function (req, res) {
            connection.connect().then(function () {
                let sqlQuery = "SELECT * FROM booking where Cancelled=1";
                let request = new sql.Request(connection);
                request.query(sqlQuery).then(function (recordset) {
                    res.json(recordset.recordset);
                    connection.close();
                })
                    .catch(function (err) {
                        console.log(err);
                        connection.close();
                        res.status(400).send("Database Error while getting data ");
                    });
            })
                .catch(function (err) {
                    connection.close();
                    res.status(400).send("Connection Error while getting data " + req.body.Passengers);
                });
        });



    router.route('/booking/byDate/:mm/:dd/:yyyy')       //getting list of booking on a particular date
        .get(function (req, res) {
            let dateOut = `${req.params.yyyy}-${req.params.mm}-${req.params.dd}`;
            connection.connect().then(function () {
                let sqlQuery = "SELECT * FROM dbo.Booking WHERE DateOut = @dateOut";
                let req = new sql.Request(connection);
                req.input("DateOut", sql.Date(), dateOut);
                req.query(sqlQuery).then(function (recordset) {
                    res.json(recordset.recordset);
                    connection.close();
                })
                    .catch(function (err) {
                        connection.close();
                        res.status(400).send("Database Error while getting data");
                        console.log(err);
                    });
            })
                .catch(function (err) {
                    connection.close();
                    res.status(400).send("Connection Error while getting data " + err);
                });
        });

    router.route('/booking/byMonth/:mm/:yyyy')       //getting list of bookings for a particular month
        .get(function (req, res) {
            let monthBegin = `${req.params.yyyy}-${req.params.mm}-1`;
            let monthEnd = `${req.params.yyyy}-${req.params.mm}-31`;
            connection.connect().then(function () {
                let sqlQuery = "SELECT * FROM dbo.Booking WHERE DateOut >= @MonthBegin AND DateOut <= @MonthEnd";
                let req = new sql.Request(connection);
                req.input("MonthBegin", sql.Date(), monthBegin);
                req.input("MonthEnd", sql.Date(), monthEnd);
                req.query(sqlQuery).then(function (recordset) {
                    res.json(recordset.recordset);
                    connection.close();
                })
                    .catch(function (err) {
                        connection.close();
                        res.status(400).send("Database Error while getting data");
                        console.log(err);
                    });
            })
                .catch(function (err) {
                    connection.close();
                    res.status(400).send("Connection Error while getting data " + err);
                });
        });


    router.route('/booking/byYear/:yyyy')    //getting list of bookings for a particular year
        .get(function (req, res) {
            let yearBegin = `${req.params.yyyy}-1-1`;
            let yearEnd = `${req.params.yyyy}-12-31`;
            connection.connect().then(function () {
                let sqlQuery = "SELECT * FROM dbo.Booking WHERE DateOut >= @YearBegin AND DateOut <= @YearEnd";
                let req = new sql.Request(connection);
                req.input("YearBegin", sql.Date(), yearBegin);
                req.input("YearEnd", sql.Date(), yearEnd);
                req.query(sqlQuery).then(function (recordset) {
                    res.json(recordset.recordset);
                    connection.close();
                })
                    .catch(function (err) {
                        connection.close();
                        res.status(400).send("Database Error while getting data");
                        console.log(err);
                    });
            })
                .catch(function (err) {
                    connection.close();
                    res.status(400).send("Connection Error while getting data " + err);
                });
        });

    router.route('/booking/nextWeek/')    //getting bookings for a week
        .get(function (req, res) {
            let weekBegin = new Date();
            let weekEnd = new Date();
            weekEnd.setDate(weekEnd.getDate() + 7);
            weekEnd.setHours(0, 0, 0, 0);
            connection.connect().then(function () {
                let sqlQuery = "SELECT * FROM dbo.Booking WHERE DateOut >= @WeekBegin AND DateOut <= @WeekEnd";
                let req = new sql.Request(connection);
                req.input("WeekBegin", sql.Date(), weekBegin);
                req.input("WeekEnd", sql.Date(), weekEnd);
                req.query(sqlQuery).then(function (recordset) {
                    res.json(recordset.recordset);
                    connection.close();
                })
                    .catch(function (err) {
                        connection.close();
                        res.status(400).send("Database Error while getting data");
                        console.log(err);
                    });
            })
                .catch(function (err) {
                    connection.close();
                    res.status(400).send("Connection Error while getting data " + err);
                });
        });

    router.route('/booking/nextMonth/')  //getting bookinf from today's date till next month
        .get(function (req, res) {
            let monthBegin = new Date();
            let monthEnd = new Date();
            monthEnd.setDate(monthEnd.getDate() + 31);
            monthEnd.setHours(23, 59, 59, 999);
            connection.connect().then(function () {
                let sqlQuery = "SELECT * FROM dbo.Booking WHERE DateOut >= @MonthBegin AND DateOut <= @MonthEnd";
                let req = new sql.Request(connection);
                req.input("MonthBegin", sql.Date(), monthBegin);
                req.input("MonthEnd", sql.Date(), monthEnd);
                req.query(sqlQuery).then(function (recordset) {
                    res.json(recordset.recordset);
                    connection.close();
                })
                    .catch(function (err) {
                        connection.close();
                        res.status(400).send("Database Error while getting data");
                        console.log(err);
                    });
            })

                .catch(function (err) {
                    connection.close();
                    res.status(400).send("Connection Error while getting data " + err);
                });
        });

    router.route('/booking/nextSixMonths/')      //getting bookinf from today's date till next six months
        .get(function (req, res) {
            let sixMonthBegin = new Date();
            let sixMonthEnd = new Date();
            sixMonthEnd.setDate(sixMonthBegin.getDate() + 186);
            sixMonthEnd.setHours(0, 0, 0, 0);
            connection.connect().then(function () {
                let sqlQuery = "SELECT * FROM dbo.Booking WHERE DateOut >= @SixMonthBegin AND DateOut <= @SixMonthEnd";
                let req = new sql.Request(connection);
                req.input("SixMonthEnd", sql.Date(), sixMonthEnd);
                req.input("SixMonthBegin", sql.Date(), sixMonthBegin);
                req.query(sqlQuery).then(function (recordset) {
                    res.json(recordset.recordset);
                    connection.close();
                })
                    .catch(function (err) {
                        connection.close();
                        res.status(400).send("Database Error while getting data");
                        console.log(err);
                    });
            })
                .catch(function (err) {
                    connection.close();
                    res.status(400).send("Connection Error while getting data " + err);
                });
        });

    router.route('/booking/lastWeek/') //getting bookinf from today's date to last week
        .get(function (req, res) {
            let weekEnd = new Date();
            let weekBegin = new Date();
            weekBegin.setDate(weekBegin.getDate() - 7);
            weekBegin.setHours(0, 0, 0, 0);
            connection.connect().then(function () {
                let sqlQuery = "SELECT * FROM dbo.Booking WHERE DateOut >= @WeekBegin AND DateOut <= @WeekEnd";
                let req = new sql.Request(connection);
                req.input("WeekBegin", sql.Date(), weekBegin);
                req.input("WeekEnd", sql.Date(), weekEnd);
                req.query(sqlQuery).then(function (recordset) {
                    res.json(recordset.recordset);
                    connection.close();
                })
                    .catch(function (err) {
                        connection.close();
                        res.status(400).send("Database Error while getting data");
                        console.log(err);
                    });
            })
                .catch(function (err) {
                    connection.close();
                    res.status(400).send("Connection Error while getting data " + err);
                });
        });    



    router.route('/booking/lastMonth/')  //getting bookinf from today's date to last month
        .get(function (req, res) {
            let monthEnd = new Date();
            let monthBegin = new Date();
            monthBegin.setDate(monthBegin.getDate() - 31);
            monthBegin.setHours(0, 0, 0, 0);
            connection.connect().then(function () {
                let sqlQuery = "SELECT * FROM dbo.Booking WHERE DateOut >= @MonthBegin AND DateOut <= @MonthEnd";
                let req = new sql.Request(connection);
                req.input("MonthBegin", sql.Date(), monthBegin);
                req.input("MonthEnd", sql.Date(), monthEnd);
                req.query(sqlQuery).then(function (recordset) {
                    res.json(recordset.recordset);
                    connection.close();
                    res.status(400).send("Database Error while getting data");
                    console.log(err);
                });
            })
                .catch(function (err) {
                    connection.close();
                    res.status(400).send("Connection Error while getting data " + err);
                });
        });

        router.route('/booking/lastSixMonths/')
            .get(function (req, res) {
                let sixMonthEnd = new Date();
                let sixMonthBegin = new Date();
                sixMonthBegin.setDate(sixMonthBegin.getDate() - 186);
                sixMonthBegin.setHours(0, 0, 0, 0);
                connection.connect().then(function () {
                    let sqlQuery = "SELECT * FROM dbo.Booking WHERE DateOut >= @SixMonthBegin AND DateOut <= @SixMonthEnd";
                    let req = new sql.Request(connection);
                    req.input("SixMonthBegin", sql.Date(), sixMonthBegin);
                    req.input("SixMonthEnd", sql.Date(), sixMonthEnd);
                    req.query(sqlQuery).then(function (recordset) {
                        res.json(recordset.recordset);
                        connection.close();
                    })
                        .catch(function (err) {
                            connection.close();
                            res.status(400).send("Database Error while getting data");
                            console.log(err);
                        });
                })
                    .catch(function (err) {
                        connection.close();
                        res.status(400).send("Connection Error while getting data " + err);
                    });
            });
         

    router.route('/user/:uid')       //getting all the booking data for a particular user through his user Id
        .get(function (req, res) {
            let UserId = req.params.uid;
            connection.connect().then(function () {
                let sqlQuery = "SELECT * FROM dbo.Booking WHERE UserId = @UserId order by DateOut DESC";
                let req = new sql.Request(connection);
                req.input("UserId", sql.Int(5), UserId);
                req.query(sqlQuery).then(function (recordset) {
                    res.json(recordset.recordset);
                    connection.close();
                })
                    .catch(function (err) {
                        connection.close();
                        res.status(400).send("Database Error while getting data");
                        console.log(err);
                    });
            })
                .catch(function (err) {
                    connection.close();
                    res.status(400).send("Database Error while getting data");
                    console.log(err);
                });
        });

    router.route('/flight/:fid')      //getting all the booking data for a particular flight through flight Id
        .get(function (req, res) {
            let FlightOutId = req.params.fid;
            connection.connect().then(function () {
                let sqlQuery = "SELECT * FROM dbo.Booking WHERE FlightOutId = @FlightOutId ";
                let req = new sql.Request(connection);
                req.input("FlightOutId", sql.Int(5), FlightOutId);
                req.query(sqlQuery).then(function (recordset) {
                    res.json(recordset.recordset);
                    connection.close();
                })
                    .catch(function (err) {
                        connection.close();
                        res.status(400).send("Database Error while getting data");
                        console.log(err);
                    });
            })
                .catch(function (err) {
                    connection.close();
                    res.status(400).send("Database Error while getting data");
                    console.log(err);
                });
        });


    router.route('/user/cancel/:uid')      //getting all the cancelled booking data for a particular user through his user Id
        .get(function (req, res) {
            let UserId = req.params.uid;
            connection.connect().then(function () {
                let sqlQuery = "SELECT * FROM dbo.Booking WHERE UserId = @UserId AND Cancelled=1";
                let req = new sql.Request(connection);
                req.input("UserId", sql.Int(5), UserId);
                req.query(sqlQuery).then(function (recordset) {
                    res.json(recordset.recordset);
                    connection.close();
                })
                    .catch(function (err) {
                        connection.close();
                        res.status(400).send("Database Error while getting data");
                        console.log(err);
                    });
            })
                .catch(function (err) {
                    connection.close();
                    res.status(400).send("Connection Error while getting data " + err);
                });
        });

    return router;
};
module.exports = routes;

var express = require('express');
var app = express();
const bodyParser = require('body-parser');
var sql = require("mssql");
const contactsController = require('./controller/controller')();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use((request, response, next) => {
  response.header("Access-Control-Allow-Origin", "*");
  response.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
app.use("/api", contactsController);

var server = app.listen(5000, function () {
    console.log('Server is running..');
});

module.exports = app;

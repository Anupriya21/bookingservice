var assert = require('assert');
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');
const expect = chai.expect;
const should = chai.should();
chai.use(chaiHttp);

//test get all
describe("Test to check that we are getting all the bookings ", () => {
    it('should list ALL bookings on /home GET', function (done) {
        chai.request(server)
            .get('/api/booking')
            .end(function (err, res) {
                res.should.have.status(200);
                //res.body.length.should.be.eql(0);
                done();
            });
    });

});

describe('Test for checking that no empty values are inserted', () => {
    it('A test to check it allows no missing values to be inserted', (done) => {
        let booking = {
            DateOut: "2014-01-01",
            Passengers: "11"

        }
        chai.request(server)
            .post('/api/booking')
            .send(booking)
            .end((err, res) => {
                res.should.have.status(401);
                done();
            });
    });

    it('A passed test which has all the input values in the right format', (done) => {
        let booking = {
            DateOut: "2019-09-01",
            DateBack: "2019-10-03",
            Passengers: "7",
            FlightOutId: "v1234",
            FlightBackId: "v2345",
            ReturnOneway: "true",
            UserId: "-31",
            BookingPrice: "1234.21",
            PaymentId: "5"

        }
        chai.request(server)
            .post('/api/booking')
            .send(booking)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.have.property('Passengers');
                res.body.should.have.property('DateOut');
                res.body.should.have.property('DateBack');
                res.body.should.have.property('FlightOutId');
                res.body.should.have.property('FlightBackId');
                res.body.should.have.property('ReturnOneway');
                res.body.should.have.property('UserId');
                res.body.should.have.property('BookingPrice');
                res.body.should.have.property('PaymentId');
                done();
            });
    });
    it('checking the maximum number of passengers allowed', (done) => {
        let booking = {
            DateOut: "2014-01-01",
            DateBack: "2014-01-03",
            Passengers: "21"

        }
        chai.request(server)
            .post('/api/booking')
            .send(booking)
            .end((err, res) => {
                res.should.have.status(401);
                done();
            });
    });

    it('A test to check if all cancelled flights are returned successfully by the database', (done) => {
        chai.request(server)
            .get('/api/booking/cancelled')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });

});

describe("GET all bookings for a date, month or year", () => {
    it(' testing with mm-dd-yyyy work', function (done) {
        chai.request(server)
            .get('/api/booking/byDate/11/04/2019')
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.above(0);
                done();
            });
    });
    it(' testing with just month and a year', function (done) {
        chai.request(server)
            .get('/api/booking/byMonth/02/2018')
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.above(0);
                done();
            });
    });
    it(' testing with just a year', function (done) {
        chai.request(server)
            .get('/api/booking/byYear/2018')
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.above(0);
                done();
            });
    });
});

describe("Get all bookings for the next week/month/6months", () => {
    it(' Next week ', function (done) {
        chai.request(server)
            .get('/api/booking/nextWeek/')
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.above(0);
                done();
            });
    });
    it(' Next Month ', function (done) {
        chai.request(server)
            .get('/api/booking/nextMonth/')
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.above(0);
                done();
            });
    });
    it(' Next six months', function (done) {
        chai.request(server)
            .get('/api/booking/nextSixMonths/')
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.above(0);
                done();
            });
    });
});

describe("Get all bookings for the last week/month/6months", () => {
    it(' Last week ', function (done) {
        chai.request(server)
            .get('/api/booking/lastWeek/')
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.above(0);
                done();
            });
    });
    it(' Last Month ', function (done) {
        chai.request(server)
            .get('/api/booking/lastMonth/')
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.above(0);
                done();
            });
    });
    it(' Last six months', function (done) {
        chai.request(server)
            .get('/api/booking/lastSixMonths/')
            .end(function (err, res) {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.above(0);
                done();
            });
    });
});
